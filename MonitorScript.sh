#!/bin/bash


# Usage: .MonitorScript.sh

# The script checks:
# - If a container is running.
# - Container resources
# - Microservice status

IFS=;
GREEN='\033[0;32m'
RED='\033[0;31m'
GRAY='\033[0;37m'
BLUE='\033[0;34m'
NC='\033[0m'
MICRO_CONTAINER_NAME='mydevops_challenge_microservice_1'
DB_CONTAINER_NAME='mydevops_challenge_couchdb_1'


MICRO_ID=$(docker ps -aqf "name=$MICRO_CONTAINER_NAME")
DB_ID=$(docker ps -aqf "name=$DB_CONTAINER_NAME")
RUNNING_MICRO=$(docker inspect --format="{{ .State.Running }}" $MICRO_ID 2> /dev/null)
RUNNING_DB=$(docker inspect --format="{{ .State.Running }}" $DB_ID 2> /dev/null)

echo -e "${GRAY}Checking status: ${NC}"

if [ "$RUNNING_MICRO" == "false" ]; then
  echo -e "${RED}ERROR${NC} - $MICRO_ID is not running."
  exit 2
else
STARTED_MICRO=$(docker inspect --format="{{ .State.StartedAt }}" $MICRO_ID)
NAME_MICRO=$(docker inspect --format="{{ .Name }}" $MICRO_ID)
echo -e "${GREEN}OK ${NC}- ${BLUE}$NAME_MICRO${NC} is ${GREEN}running.${NC} \tStartedAt: $STARTED_MICRO, \tIDCONTAINER: $MICRO_ID" | column -t -s $'\t'
fi

if [ "$RUNNING_DB" == "false" ]; then
  echo -e "${RED}ERROR${NC} - $DB_ID is not running."
  exit 2
else
STARTED_DB=$(docker inspect --format="{{ .State.StartedAt }}" $DB_ID)
NAME_DB=$(docker inspect --format="{{ .Name }}" $DB_ID)
echo -e "${GREEN}OK ${NC}- ${BLUE}$NAME_DB${NC} is ${GREEN}running.${NC} \tStartedAt: $STARTED_DB, \tIDCONTAINER: $DB_ID" | column -t -s $'\t'
fi


 




echo
echo

echo -e "${GRAY}Checking resources: ${NC}"
DOCKER_STATS=`docker stats --no-stream --format "table {{.Name}}\t{{.ID}}\t{{.MemPerc}}\t{{.CPUPerc}}\t{{.MemUsage}}"`
echo $DOCKER_STATS

echo -e "${GRAY}Disk usage${NC}"
DOCKER_SIZE=`docker ps --format "{{ .Names }}\t{{.Size}}"`
echo $DOCKER_SIZE

echo
echo

echo -e "${GRAY}Microservice status:${NC}"
MICRO_STATUS=`curl -I http://localhost:8081/v1/ui/ 2> /dev/null | head -n 1 | cut -d$" " -f2`


if [ "$MICRO_STATUS" == 200 ]; then
  echo -e "Status: ${GREEN}$MICRO_STATUS${NC} microservice is ${GREEN}running.${NC}"
  

else
echo -e "${RED}CRITICAL${NC} - $MICRO_STATUS is not running."
exit 2
fi
