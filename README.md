## DevOps Challenge
------------------
> This repository contains my solution to the DevOps Challenge. Deploy multi-container apps with Docker Compose.

### Build the microservice

Create a Dockerfile where the following was specified:

 ```
 # Base image choose a lightweight version
FROM python:alpine

# Add warehouse_service code to /code inside container image
ADD /warehouse_service /code

# Set working directory for subsequent commands
WORKDIR /code

# Install dependencies described in requirements.txt  
RUN pip install -r requirements.txt

# Command to run when container starts
ENTRYPOINT ["python", "app.py"]
```

### Deploy the microservice

Use Docker Compose for running multiple containers as a single service.
In docker-composer file create two services, one for deploy the microservice and other for deploy the database.

```
 services:
  microservice:
    build: .
    command: python app.py
    ports:
      - target: 8080
        published: 8081
    networks:
      - mychallenge-net
    volumes:
      - ./warehouse_service:/code
    depends_on:
      - couchdb
```

1. **build:** Use . for specify the previous Dockerfile
2. **command:** Command that runs after the container is created.
3. **ports:** *target* the port inside the container, *published* the publicly exposed port
4. **networks:** Set the network
5. **volumes:** Set the volume where are the app code
6. **depends_on:** Express dependency between the microservice and database.

### Deploy the database
Create other service in the same docker-compose file for database.

```
couchdb:
    image: "couchdb"
    environment:
      - COUCHDB_USER=admin
      - COUCHDB_PASSWORD=pass
    volumes:
      - ./data/couchdb:/opt/couchdb/data
    networks:
      mychallenge-net:
```

1. **image:** Specify the couchdb image.
2. **environment:** Set *USER* and *PASSWORD* when create the database.
3. **volumes:** The volume where the data saved in the database will be persisted is specified, if the container is turned off the data will persist in the file system of the local computer.
4. **networks:** Same network that microservice.

As specified in the requirements, access to the database cannot be done externally, for this reason the port is not exposed here, only containers that are in mychallenge-net will be able to access the database.

Additional is specified in the docker-compose file the name of the network and volumes.

```
networks:
  mychallenge-net:

volumes:
  mychallenge-vol:
```

### Monitor Script

**Check if the container is running**

To solve this requirement,first, set the containers name, the use the command *docker inspect* to get the status

```
MICRO_CONTAINER_NAME='mydevops_challenge_microservice_1'
RUNNING_MICRO=$(docker inspect --format="{{ .State.Running }}" $MICRO_ID 2> /dev/null)
```

**Container resources**

*docker stats* returns a live data stream for running containers, use the flag *--format* to specify the placeholder needed.
```
DOCKER_STATS=`docker stats --no-stream --format "table {{.Name}}\t{{.ID}}\t{{.MemPerc}}\t{{.CPUPerc}}\t{{.MemUsage}}"`
```
Use the *table* option to include column headers.

in the same way use the command *docker ps* to get information about the containers.
```
DOCKER_SIZE=`docker ps --format "{{ .Names }}\t{{.Size}}"`
```


**Microservice status**

Use the *cURL* whith *-I* flag to fecht the HTTP headers of the address that exposes the service.
```
MICRO_STATUS=`curl -I http://localhost:8081/v1/ui/ 2> /dev/null | head -n 1 | cut -d$" " -f2`
```

Usage
-----

Run the container.

```
docker-compose up -d
```

Run the monitor

```
./MonitorScript.sh
```


### Results

Successful deployment of both containers.

![](images/image1.png)

Correct operation using Swagger.
```
http://localhost:8081/v1/ui
```


![](images/image2.png)
![](images/image3.png)

The monitor confirms that everything works successfully.

![](images/image4.png)



